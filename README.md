### Docker postgres container

Largely copied from the docker documentation.
Creates dev_db and prod_db, owned by 
user: auric
with password: grandslam
Please change those for obvious reasons.

#### Usage
```lang=shell
$ docker build -t some-witty-name .
$ docker run --rm -P --name pg_some-witty-name some-witty-name
# Test if everything works
$ docker run --rm -t -i --link pg_some-witty-name:pg some-witty-name bash
# Inside container
$ psql -h $PG_PORT_5432_TCP_ADDR -p $PG_PORT_5432_TCP_PORT -d demo_db -U auric
# on psql commandline
demo_db=> CREATE TABLE foo (ID int, Data varchar(255));
demo_db=> DROP TABLE foo;
```
