FROM debian

RUN apt-get update && apt-get install -y postgresql

USER postgres

RUN /etc/init.d/postgresql start &&\
	psql --command "CREATE USER auric WITH PASSWORD 'grandslam';" &&\
	createdb -O auric dev_db &&\
	createdb -O auric prod_db
RUN echo "host all all	0.0.0.0/0 md5" >> /etc/postgresql/9.6/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf
EXPOSE 5432

VOLUME ["/etc/postgresql","/var/log/postgresql","/var/lib/postgresql"]

CMD ["/usr/lib/postgresql/9.6/bin/postgres", "-D", "/var/lib/postgresql/9.6/main", "-c", "config_file=/etc/postgresql/9.6/main/postgresql.conf"]
